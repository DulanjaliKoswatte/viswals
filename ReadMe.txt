1.	Run following command from Viswals folder (Project folder) to run services
$docker-compose up

If need to ensure deployment is happening perfectly, use the following steps
	1.1	Open another command prompt in same location and run the following command to check all services' status
	$docker-compose ps

	$ docker-compose ps

	Output -->
			  Name                         Command               State                                                        Ports                                                
	-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	viswals_consumer_1          java -jar /app/consumer.jar      Up      0.0.0.0:8081->8081/tcp                                                                                    
	viswals_nginx_1             /docker-entrypoint.sh ngin ...   Up      0.0.0.0:8080->80/tcp                                                                                      
	viswals_postgres_1          docker-entrypoint.sh postgres    Up      0.0.0.0:5432->5432/tcp                                                                                    
	viswals_publisher_1         java -jar /app/publisher.jar     Up      0.0.0.0:8083->8083/tcp                                                                                    
	viswals_rabbitmq-config_1   docker-entrypoint.sh rabbi ...   Up      15671/tcp, 15672/tcp, 15691/tcp, 15692/tcp, 25672/tcp, 4369/tcp, 5671/tcp, 5672/tcp                       
	viswals_rabbitmq_1          docker-entrypoint.sh rabbi ...   Up      15671/tcp, 0.0.0.0:15672->15672/tcp, 15691/tcp, 15692/tcp, 25672/tcp, 4369/tcp, 5671/tcp, 0.0.0.0:5672->5672/tcp
	viswals_redis_1             docker-entrypoint.sh redis ...   Up      0.0.0.0:6379->6379/tcp                                                                                    

	1.2. If all services are up and running check rabbitmq UI
	RabbitMQ UI : http://localhost:15672/#/

	1.3. If needed check Redis
	$docker exec -it viswals_redis_1 redis-cli -a viswals

	1.4 If need to check Postgre
	$docker exec -it viswals_postgres_1 psql -U viswals -d viswals

2. If all services are up and running, run the following to initiate users.csv processing 
$curl --location --request GET 'http://localhost:8083/processCsv'

3. Once the DB update is done, open user details UI in the browser
http://localhost:8080/viswals.html
