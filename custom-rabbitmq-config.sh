#!/bin/sh

# Wait for RabbitMQ to be ready
sleep 10

# Declare the exchange
rabbitmqctl --wait list_exchanges | grep -q 'viswals-exchange' || rabbitmqctl --wait -q declare exchange name=viswals-exchange type=direct

# Declare the queues
rabbitmqctl --wait list_queues | grep -q 'viswals-que-postgre' || rabbitmqctl --wait -q declare queue name=viswals-que-postgre
rabbitmqctl --wait list_queues | grep -q 'viswals-que-redis' || rabbitmqctl --wait -q declare queue name=viswals-que-redis

# Bind the queues to the exchange
rabbitmqctl --wait list_bindings | grep -q 'viswals-exchange viswals-que-postgre' || rabbitmqctl --wait -q bind_queue source=viswals-exchange destination=viswals-que-postgre routing_key=viswals-routing-key
rabbitmqctl --wait list_bindings | grep -q 'viswals-exchange viswals-que-redis' || rabbitmqctl --wait -q bind_queue source=viswals-exchange destination=viswals-que-redis routing_key=viswals-routing-key

# Display RabbitMQ status
rabbitmqctl list_exchanges
rabbitmqctl list_queues
rabbitmqctl list_bindings

exec "$@"
