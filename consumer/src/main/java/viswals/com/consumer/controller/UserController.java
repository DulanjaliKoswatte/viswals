package viswals.com.consumer.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import viswals.com.consumer.dto.ResponseWrapper;
import viswals.com.consumer.entity.UserDto;
import viswals.com.consumer.services.UserService;

import java.util.List;

@RestController
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }
    private static final Logger LOGGER = LogManager.getLogger(UserController.class);
    @GetMapping("/getUserDetails/v1")
    @CrossOrigin(origins = "http://localhost:8080")
    public ResponseEntity<ResponseWrapper> getUserDetails(@RequestParam(name = "id", required = false) Long id,
                                                        @RequestParam(name = "emailAddress", required = false ) String emailAddress){
        List<UserDto> users = userService.getUserDetails(id,emailAddress);
        ResponseWrapper responseWrapper = new ResponseWrapper();
        responseWrapper.setUserInfo(users);
        LOGGER.info("responseWrapper : {}",responseWrapper);
        return ResponseEntity.ok().body(responseWrapper);
    }
}
