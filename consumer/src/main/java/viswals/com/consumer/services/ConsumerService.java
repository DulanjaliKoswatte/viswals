package viswals.com.consumer.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import viswals.com.consumer.dto.User;
import viswals.com.consumer.entity.UserDto;
import viswals.com.consumer.repository.CasheRepository;
import viswals.com.consumer.repository.UserRepository;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class ConsumerService {

    private final UserRepository userRepository;
    private final CasheRepository casheRepository;

    @Qualifier("customMapping")
    private final ObjectMapper objectMapper;

    private static final Logger LOGGER = LogManager.getLogger(ConsumerService.class);

    @Autowired
    public ConsumerService(UserRepository userRepository, CasheRepository casheRepository, ObjectMapper objectMapper) {
        this.userRepository = userRepository;
        this.casheRepository = casheRepository;
        this.objectMapper = objectMapper;
    }


    private ExecutorService executorService = Executors.newFixedThreadPool(5);

    @RabbitListener(queues = "viswals_que_postgre")
    @Transactional
    public void receiveMessageForPostgre(String message) {
        executorService.execute(() -> {
            try {
                User user = objectMapper.readValue(message, User.class);
                saveMessageInPostgre(user);
                LOGGER.info("message postgre: {}", message);
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        });
    }

    public void saveMessageInPostgre(User user) {
        UserDto userDto = userRepository.insertUser(user);
        if (user.getParentUserId() != null && user.getParentUserId() != -1 && !userDto.getIsParent()) {
            userRepository.insertParentChildRelationship(user.getParentUserId(), user.getId());
        }
    }

    @RabbitListener(queues = "viswals_que_redis")
    @Transactional
    public void receiveMessageForRedis(String message) {
        executorService.execute(() -> {
            try {
                User user = objectMapper.readValue(message, User.class);
                UserDto userDto = saveMessageInRedis(user);
                LOGGER.info("message redis: {}", userDto);
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        });
    }

    public UserDto saveMessageInRedis(User user) {
        return casheRepository.saveInRedis(user);
    }
}
