package viswals.com.consumer.services;

import org.springframework.stereotype.Service;
import viswals.com.consumer.entity.UserDto;
import viswals.com.consumer.repository.CasheRepository;
import viswals.com.consumer.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final CasheRepository casheRepository;

    public UserService(UserRepository userRepository, CasheRepository casheRepository) {
        this.userRepository = userRepository;
        this.casheRepository = casheRepository;
    }

    public List<UserDto> getUserDetails(Long id, String emailAddress){
        List<UserDto> userList = new ArrayList<>();
        if(id!=null && id!=0){
            UserDto user = casheRepository.getUser(id);
            userList.add(user);
        }else if(emailAddress!=null && !emailAddress.isEmpty()){
            List<UserDto> users = userRepository.getUserData(emailAddress);
            userList = users;
        }
        return userList;
    }
}
