package viswals.com.consumer.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import viswals.com.consumer.dto.User;

import java.sql.Timestamp;
@Getter
@Setter
@NoArgsConstructor
public class UserDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private Timestamp createdAt;
    private Timestamp deletedAt;
    private Timestamp mergedAt;
    private Long parentUserId;
    private Boolean isParent;
    private Boolean isDeleted;

    public UserDto(User user) {
        this.id = user.getId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.emailAddress = user.getEmailAddress();
        this.setCreatedAt(user.getCreatedAt());
        this.setDeletedAt(user.getDeletedAt());
        this.setMergedAt(user.getMergedAt());
        this.parentUserId = user.getParentUserId();
    }
}
