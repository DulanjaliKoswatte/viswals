package viswals.com.consumer.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ParentChildRelationship {

    Long parentId;
    Long childId;
}
