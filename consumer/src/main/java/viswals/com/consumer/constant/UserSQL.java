package viswals.com.consumer.constant;

public class UserSQL {
    private UserSQL() {
        throw new IllegalStateException("Utility class");
    }
    public static final String INSERT_USER_SQL =
            "INSERT INTO users (id, first_name, last_name, email_address, created_at, deleted_at, merged_at,parent_user_id, is_parent, is_deleted) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    public static final String SELECT_USER_BY_ID_SQL =
            "SELECT id, first_name, last_name, email_address, created_at, deleted_at, merged_at, parent_user_id, is_parent, is_deleted FROM users WHERE id = ?";

    public static final String SELECT_USER_BY_EMAIL_SQL =
            "SELECT id, first_name, last_name, email_address, created_at, deleted_at, merged_at, parent_user_id, is_parent, is_deleted FROM users WHERE email_address = ?";

    public static final String INSERT_PARENT_CHILD_RELATIONSHIP_SQL =
            "INSERT INTO parent_child_relationship (parent_id, child_id) VALUES (?, ?)";

    public static final String SELECT_CHILDREN_OF_PARENT_SQL =
            "SELECT u.id, u.first_name, u.last_name " +
                    "FROM users u " +
                    "INNER JOIN parent_child_relationship pcr ON u.id = pcr.child_id " +
                    "WHERE pcr.parent_id = ?";
}
