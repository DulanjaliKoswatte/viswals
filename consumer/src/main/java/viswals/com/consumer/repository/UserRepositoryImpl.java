package viswals.com.consumer.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import viswals.com.consumer.constant.UserSQL;
import viswals.com.consumer.dto.User;
import viswals.com.consumer.entity.UserDto;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final JdbcTemplate jdbcTemplate;
    private final CommonRepository commonRepository;
    private static final Logger LOGGER = LogManager.getLogger(UserRepositoryImpl.class);
    @Autowired
    public UserRepositoryImpl(JdbcTemplate jdbcTemplate, CommonRepository commonRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.commonRepository = commonRepository;
    }




    public UserDto insertUser(User user) {
        UserDto userDto = commonRepository.createdto(user);

        jdbcTemplate.update(UserSQL.INSERT_USER_SQL,
                userDto.getId(),
                userDto.getFirstName(),
                userDto.getLastName(),
                userDto.getEmailAddress(),
                userDto.getCreatedAt(),
                userDto.getDeletedAt(),
                userDto.getMergedAt(),
                userDto.getParentUserId(),
                userDto.getIsDeleted(),
                userDto.getIsParent());
        return  userDto;
    }

    public void insertParentChildRelationship(Long parentId, Long childId) {
        jdbcTemplate.update(UserSQL.INSERT_PARENT_CHILD_RELATIONSHIP_SQL, parentId, childId);
    }


    public UserDto getUserData(Long id) {
        UserDto user = new UserDto();
        jdbcTemplate.query(UserSQL.SELECT_USER_BY_ID_SQL,(resultSet, rowNum) -> {

             user.setId(resultSet.getLong("id"));
             user.setFirstName(resultSet.getString("first_name"));
             user.setLastName(resultSet.getString("last_name"));
             user.setEmailAddress(resultSet.getString("email_address"));
             user.setCreatedAt(resultSet.getTimestamp("created_at"));
             user.setDeletedAt(resultSet.getTimestamp("deleted_at") != null ?
                     resultSet.getTimestamp("deleted_at") : null);
             user.setMergedAt(resultSet.getTimestamp("merged_at") != null ?
                     resultSet.getTimestamp("merged_at") : null);
             LOGGER.info("user : {}",user);
             return user;
         },id);
        return user;
    }

    public List<UserDto> getUserData(String emailAddress) {
        List<UserDto> users = new ArrayList<>();
        jdbcTemplate.query(UserSQL.SELECT_USER_BY_EMAIL_SQL,(resultSet, rowNum) -> {
            UserDto user = new UserDto();
            user.setId(resultSet.getLong("id"));
            user.setFirstName(resultSet.getString("first_name"));
            user.setLastName(resultSet.getString("last_name"));
            user.setEmailAddress(resultSet.getString("email_address"));
            user.setCreatedAt(resultSet.getTimestamp("created_at"));
            user.setDeletedAt(resultSet.getTimestamp("deleted_at") != null ?
                    resultSet.getTimestamp("deleted_at") : null);
            user.setMergedAt(resultSet.getTimestamp("merged_at") != null ?
                    resultSet.getTimestamp("merged_at") : null);
            user.setParentUserId(resultSet.getLong("parent_user_id"));
            user.setIsParent(resultSet.getBoolean("is_parent"));
            user.setIsDeleted(resultSet.getBoolean("is_deleted"));
            users.add(user);
            return user;
        },emailAddress);
        return users;
    }
}
