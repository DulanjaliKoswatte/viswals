package viswals.com.consumer.repository;

import org.springframework.stereotype.Component;
import viswals.com.consumer.dto.User;
import viswals.com.consumer.entity.UserDto;

@Component
public class CommonRepository {
    public UserDto createdto(User user){
        UserDto userDto = new UserDto(user);
        userDto.setIsDeleted(user.getDeletedAt() != null);
        userDto.setIsParent(user.getParentUserId() != null && user.getId().equals( user.getParentUserId()));
        return userDto;
    }

}
