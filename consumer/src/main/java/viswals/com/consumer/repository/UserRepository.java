package viswals.com.consumer.repository;

import viswals.com.consumer.dto.User;
import viswals.com.consumer.entity.UserDto;

import java.util.List;

public interface UserRepository {
    UserDto insertUser(User user);
    void insertParentChildRelationship(Long parentId, Long childId);

    UserDto getUserData(Long id);
    List<UserDto> getUserData(String emailAddress);
}
