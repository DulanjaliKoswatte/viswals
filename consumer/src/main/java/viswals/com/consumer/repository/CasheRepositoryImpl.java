package viswals.com.consumer.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import viswals.com.consumer.dto.User;
import viswals.com.consumer.entity.UserDto;

@Repository
public class CasheRepositoryImpl implements CasheRepository{
    private final RedisTemplate<Long, UserDto> redisTemplate;
    private final CommonRepository commonRepository;

    @Autowired
    public CasheRepositoryImpl(RedisTemplate<Long, UserDto> redisTemplate, CommonRepository commonRepository) {
        this.redisTemplate = redisTemplate;
        this.commonRepository = commonRepository;
    }

    @Override
    public UserDto saveInRedis(User user) {
        UserDto userDto= commonRepository.createdto(user);
        redisTemplate.opsForValue().set(userDto.getId(),userDto);
        return userDto;
    }

    @Override
    public UserDto getUser(Long id) {
        return redisTemplate.opsForValue().get(id);
    }
}
