package viswals.com.consumer.repository;

import viswals.com.consumer.dto.User;
import viswals.com.consumer.entity.UserDto;

public interface CasheRepository {
     UserDto saveInRedis(User user);
     UserDto getUser(Long id);
}
