package viswals.com.consumer.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;


@Getter
@Setter
@NoArgsConstructor
public class User {
    private Long id;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private Timestamp createdAt;
    private Timestamp deletedAt;
    private Timestamp mergedAt;
    private Long parentUserId;


}
