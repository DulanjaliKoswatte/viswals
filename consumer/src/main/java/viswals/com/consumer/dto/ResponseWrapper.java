package viswals.com.consumer.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import viswals.com.consumer.entity.UserDto;

import java.util.List;
@Getter
@Setter
@NoArgsConstructor
public class ResponseWrapper {
    private List<UserDto> userInfo;
}
