-- Create the schema if it doesn't exist
CREATE SCHEMA IF NOT EXISTS viswals;
DROP TABLE IF EXISTS viswals.users;
DROP TABLE IF EXISTS viswals.parent_child_relationship;

-- Create a table in the schema
CREATE TABLE viswals.users (
                                       id SERIAL PRIMARY KEY,
                                       first_name VARCHAR(255) NOT NULL,
                                       last_name VARCHAR(255) NOT NULL,
                                       email_address VARCHAR(255) NOT NULL,
                                       created_at TIMESTAMP,
                                       deleted_at TIMESTAMP,
                                       merged_at TIMESTAMP,
                                       parent_user_id BIGINT,
                                       is_parent BOOLEAN,
                                       is_deleted BOOLEAN
);

CREATE TABLE viswals.parent_child_relationship (
                                                           parent_id BIGINT NOT NULL,
                                                           child_id BIGINT NOT NULL,
                                                           PRIMARY KEY (parent_id, child_id)
);
