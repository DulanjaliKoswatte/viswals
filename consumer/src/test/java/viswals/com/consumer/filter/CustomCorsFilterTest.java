package viswals.com.consumer.filter;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class CustomCorsFilterTest {

    private CustomCorsFilter customCorsFilter;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private FilterChain filterChain;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        customCorsFilter = new CustomCorsFilter();
    }

    @Test
    public void testDoFilter() throws IOException, ServletException {
        HttpServletResponse httpResponse = mock(HttpServletResponse.class);
        when(httpResponse.getHeader("Access-Control-Allow-Origin")).thenReturn("http://localhost:8080");
        customCorsFilter.doFilter(request, httpResponse, filterChain);
        verify(filterChain).doFilter(any(), any());
        assertEquals("http://localhost:8080", httpResponse.getHeader("Access-Control-Allow-Origin"));

    }
}
