package viswals.com.consumer.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import viswals.com.consumer.constant.UserSQL;
import viswals.com.consumer.dto.User;
import viswals.com.consumer.entity.UserDto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class UserRepositoryImplTest {

    @InjectMocks
    private UserRepositoryImpl userRepository;

    @Mock
    private JdbcTemplate jdbcTemplate;

    @Mock
    private CommonRepository commonRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testInsertUser() {
        // Create a sample User object
        User user = new User();
        user.setFirstName("John");
        user.setLastName("Doe");
        user.setEmailAddress("john.doe@example.com");
        user.setCreatedAt(Timestamp.valueOf("2023-01-01 00:00:00"));
        UserDto expectedUserDto = new UserDto();
        expectedUserDto.setId(1L);
        expectedUserDto.setFirstName("John");
        expectedUserDto.setLastName("Doe");
        expectedUserDto.setEmailAddress("john.doe@example.com");
        expectedUserDto.setCreatedAt(Timestamp.valueOf("2023-01-01 00:00:00"));
        when(commonRepository.createdto(user)).thenReturn(expectedUserDto);
        UserDto result = userRepository.insertUser(user);
        verify(commonRepository).createdto(user);
        verify(jdbcTemplate).update(eq(UserSQL.INSERT_USER_SQL),
                eq(expectedUserDto.getId()),
                eq(expectedUserDto.getFirstName()),
                eq(expectedUserDto.getLastName()),
                eq(expectedUserDto.getEmailAddress()),
                eq(expectedUserDto.getCreatedAt()),
                eq(expectedUserDto.getDeletedAt()),
                eq(expectedUserDto.getMergedAt()),
                eq(expectedUserDto.getParentUserId()),
                eq(expectedUserDto.getIsDeleted()),
                eq(expectedUserDto.getIsParent()));

        assertNotNull(result);
        assertEquals(expectedUserDto, result);
    }

    @Test
    public void testInsertParentChildRelationship() {
        Long parentId = 1L;
        Long childId = 2L;
        userRepository.insertParentChildRelationship(parentId, childId);
        verify(jdbcTemplate).update(eq(UserSQL.INSERT_PARENT_CHILD_RELATIONSHIP_SQL), eq(parentId), eq(childId));
    }


    @Test
    public void testGetUserDataById() {
        Long userId = 1L;


        UserDto expectedUserDto = new UserDto();
        expectedUserDto.setId(userId);
        List<UserDto> createUserDtoList = new ArrayList<>();
        createUserDtoList.add(expectedUserDto);

        when(jdbcTemplate.query(
                eq("SELECT id, first_name, last_name, email_address, created_at, deleted_at, merged_at, parent_user_id, is_parent, is_deleted FROM users WHERE id = ?"),
                any(RowMapper.class),
                eq(userId)
        )).thenReturn(createUserDtoList);


        UserDto result = userRepository.getUserData(userId);


        verify(jdbcTemplate).query(
                eq("SELECT id, first_name, last_name, email_address, created_at, deleted_at, merged_at, parent_user_id, is_parent, is_deleted FROM users WHERE id = ?"),
                any(RowMapper.class),
                eq(userId)
        );

    }


    @Test
    public void testGetUserData() {
        String emailAddress = "test@example.com";

        ResultSet resultSet = mock(ResultSet.class);
        try {
            when(resultSet.getLong("id")).thenReturn(1L);
            when(resultSet.getString("first_name")).thenReturn("John");
            when(resultSet.getString("last_name")).thenReturn("Doe");
            when(resultSet.getString("email_address")).thenReturn(emailAddress);
            when(resultSet.getTimestamp("created_at")).thenReturn(Timestamp.valueOf("2023-01-01 00:00:00"));
            when(resultSet.getTimestamp("deleted_at")).thenReturn(null);
            when(resultSet.getTimestamp("merged_at")).thenReturn(null);
            when(resultSet.getLong("parent_user_id")).thenReturn(0L);
            when(resultSet.getBoolean("is_parent")).thenReturn(false);
            when(resultSet.getBoolean("is_deleted")).thenReturn(false);
            when(resultSet.next()).thenReturn(true, false);

            when(jdbcTemplate.query(anyString(), any(RowMapper.class), eq(emailAddress)))
                    .thenAnswer(invocation -> {
                        RowMapper<UserDto> rowMapper = invocation.getArgument(1);
                        List<UserDto> users = new ArrayList<>();
                        while (resultSet.next()) {
                            users.add(rowMapper.mapRow(resultSet, 0));
                        }
                        return users;
                    });
        } catch (SQLException e) {
            e.printStackTrace();
        }

        List<UserDto> result = userRepository.getUserData(emailAddress);
        verify(jdbcTemplate).query(eq(UserSQL.SELECT_USER_BY_EMAIL_SQL), any(RowMapper.class), eq(emailAddress));
        assertNotNull(result);
        assertEquals(1, result.size());
    }
}
