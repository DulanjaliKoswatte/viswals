package viswals.com.consumer.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import viswals.com.consumer.dto.User;
import viswals.com.consumer.entity.UserDto;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class CasheRepositoryImplTest {

    @InjectMocks
    private CasheRepositoryImpl casheRepository;

    @Mock
    private RedisTemplate<Long, UserDto> redisTemplate;

    @Mock
    private CommonRepository commonRepository;

    @Mock
    private ValueOperations<Long, UserDto> valueOperations;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
    }

    @Test
    void testSaveInRedis() {
        User user = new User();
        UserDto userDto = new UserDto();
        userDto.setId(1L);
        when(commonRepository.createdto(user)).thenReturn(userDto);
        doNothing().when(valueOperations).set(userDto.getId(), userDto);
        UserDto result = casheRepository.saveInRedis(user);
        verify(commonRepository).createdto(user);
        verify(valueOperations).set(userDto.getId(), userDto);
        assertEquals(userDto, result);
    }

    @Test
    void testGetUser() {
        Long id = 1L;
        UserDto userDto = new UserDto();
        when(valueOperations.get(id)).thenReturn(userDto);
        UserDto result = casheRepository.getUser(id);
        verify(valueOperations).get(id);
        assertEquals(userDto, result);
    }
}
