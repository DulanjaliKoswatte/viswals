package viswals.com.consumer.entity;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ParentChildRelationshipTest {

    private ParentChildRelationship relationship;

    @BeforeEach
    public void setUp() {
        relationship = new ParentChildRelationship();
    }

    @Test
    public void testGetSetParentId() {
        Long parentId = 1L;
        relationship.setParentId(parentId);
        assertEquals(parentId, relationship.getParentId());
    }

    @Test
    public void testGetSetChildId() {
        Long childId = 2L;
        relationship.setChildId(childId);
        assertEquals(childId, relationship.getChildId());
    }

    @Test
    public void testEqualsAndHashCode() {
        ParentChildRelationship relationship1 = new ParentChildRelationship();
        ParentChildRelationship relationship2 = new ParentChildRelationship();

        relationship1.setParentId(1L);
        relationship1.setChildId(2L);
        relationship2.setParentId(1L);
        relationship2.setChildId(2L);

        assertEquals(relationship1.getChildId(), relationship2.getChildId());
        assertEquals(relationship1.getParentId(), relationship2.getParentId());
        relationship2.setParentId(3L);

        assertNotEquals(relationship1, relationship2);
        assertNotEquals(relationship1.hashCode(), relationship2.hashCode());
    }
}
