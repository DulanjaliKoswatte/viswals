package viswals.com.consumer.entity;

import org.junit.jupiter.api.Test;
import viswals.com.consumer.dto.User;

import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class UserDtoTest {

    @Test
    public void testUserDtoConstructor() {
        User user = new User();
        user.setId(1L);
        user.setFirstName("John");
        user.setLastName("Doe");
        user.setEmailAddress("johndoe@example.com");
        user.setCreatedAt(new Timestamp(System.currentTimeMillis()));
        user.setDeletedAt(null);
        user.setMergedAt(null);
        user.setParentUserId(0L);



        UserDto userDto = new UserDto(user);
        assertEquals(user.getId(), userDto.getId());
        assertEquals(user.getFirstName(), userDto.getFirstName());
        assertEquals(user.getLastName(), userDto.getLastName());
        assertEquals(user.getEmailAddress(), userDto.getEmailAddress());
        assertEquals(user.getCreatedAt(), userDto.getCreatedAt());
        assertNull(userDto.getDeletedAt());
        assertNull(userDto.getMergedAt());
        assertEquals(user.getParentUserId(), userDto.getParentUserId());

    }

    @Test
    public void testUserDtoDefaultConstructor() {
        UserDto userDto = new UserDto();
        assertNull(userDto.getId());
        assertNull(userDto.getFirstName());
        assertNull(userDto.getLastName());
        assertNull(userDto.getEmailAddress());
        assertNull(userDto.getCreatedAt());
        assertNull(userDto.getDeletedAt());
        assertNull(userDto.getMergedAt());
        assertNull(userDto.getParentUserId());
        assertNull(userDto.getIsParent());
        assertNull(userDto.getIsDeleted());
    }
}
