package viswals.com.consumer.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import viswals.com.consumer.dto.User;
import viswals.com.consumer.entity.UserDto;
import viswals.com.consumer.repository.CasheRepository;
import viswals.com.consumer.repository.UserRepository;

import static org.mockito.Mockito.*;

public class ConsumerServiceTest {

    @InjectMocks
    private ConsumerService consumerService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private CasheRepository casheRepository;

    @Mock
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testReceiveMessageForPostgre() throws Exception {
        User user = new User();
        user.setId(1L);
        user.setFirstName("John");
        user.setLastName("Doe");
        String message = "JSON representation of the user";
        when(objectMapper.readValue(message, User.class)).thenReturn(user);
        consumerService.receiveMessageForPostgre(message);
        //verify(userRepository, times(1)).insertUser(user);
        //verify(userRepository, times(1)).insertParentChildRelationship(user.getParentUserId(), user.getId());
    }

    @Test
    public void testReceiveMessageForRedis() throws Exception {
        User user = new User();
        user.setId(1L);
        user.setFirstName("John");
        user.setLastName("Doe");
        String message = "JSON representation of the user";
        when(objectMapper.readValue(message, User.class)).thenReturn(user);
        UserDto userDto = new UserDto();
        when(casheRepository.saveInRedis(user)).thenReturn(userDto);
        consumerService.receiveMessageForRedis(message);
       // verify(casheRepository, times(1)).saveInRedis(user);
    }
}
