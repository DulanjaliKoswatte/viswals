package viswals.com.consumer.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import viswals.com.consumer.entity.UserDto;
import viswals.com.consumer.repository.CasheRepository;
import viswals.com.consumer.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserServiceTest {
    @Mock
    private UserRepository userRepository;

    @Mock
    private CasheRepository casheRepository;

    @InjectMocks
    private UserService userService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetUserDetailsById() {
        Long userId = 62L;
        UserDto expectedUser = new UserDto();
        expectedUser.setId(userId);

        when(casheRepository.getUser(userId)).thenReturn(expectedUser);

        List<UserDto> result = userService.getUserDetails(userId, null);

        assertEquals(1, result.size());
        assertEquals(expectedUser, result.get(0));

        verify(casheRepository, times(1)).getUser(userId);
        verify(userRepository, never()).getUserData(anyString());
    }

    @Test
    public void testGetUserDetailsByEmail() {
        String emailAddress = "JanelleDubois@gmail.biz";
        List<UserDto> expectedUsers = new ArrayList<>();
        UserDto user1 = new UserDto();
        user1.setId(1L);
        UserDto user2 = new UserDto();
        user2.setId(2L);
        expectedUsers.add(user1);
        expectedUsers.add(user2);

        when(userRepository.getUserData(emailAddress)).thenReturn(expectedUsers);

        List<UserDto> result = userService.getUserDetails(null, emailAddress);

        assertEquals(2, result.size());
        assertEquals(expectedUsers, result);

        verify(userRepository, times(1)).getUserData(emailAddress);
        verify(casheRepository, never()).getUser(anyLong());
    }

    @Test
    public void testGetUserDetailsWithNoIdOrEmail() {
        List<UserDto> result = userService.getUserDetails(null, null);

        assertEquals(0, result.size());

        verify(casheRepository, never()).getUser(anyLong());
        verify(userRepository, never()).getUserData(anyString());
    }
}