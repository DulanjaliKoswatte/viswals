package viswals.com.consumer.controller;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;
import viswals.com.consumer.dto.ResponseWrapper;
import viswals.com.consumer.entity.UserDto;
import viswals.com.consumer.services.UserService;

import java.util.ArrayList;
import java.util.List;

public class UserControllerTest {

    private UserController userController;
    private UserService userService;

    @BeforeEach
    public void setUp() {
        userService = mock(UserService.class);
        userController = new UserController(userService);
    }

    @Test
    public void testGetUserDetailsById() {
        // Arrange
        Long userId = 1L;
        List<UserDto> userDtos = new ArrayList<>();
        UserDto userDto = new UserDto();
        userDto.setId(userId);
        userDtos.add(userDto);

        when(userService.getUserDetails(userId, null)).thenReturn(userDtos);

        ResponseEntity<ResponseWrapper> response = userController.getUserDetails(userId, null);


        assertEquals(200, response.getStatusCodeValue());
        ResponseWrapper responseWrapper = response.getBody();
        assertNotNull(responseWrapper);
        List<UserDto> responseUserDtos = responseWrapper.getUserInfo();
        assertNotNull(responseUserDtos);
        assertEquals(1, responseUserDtos.size());
        assertEquals(userId, responseUserDtos.get(0).getId());
    }

    @Test
    public void testGetUserDetailsByEmail() {

        String emailAddress = "test@example.com";
        List<UserDto> userDtos = new ArrayList<>();
        UserDto userDto = new UserDto();
        userDto.setEmailAddress(emailAddress);
        userDtos.add(userDto);

        when(userService.getUserDetails(null, emailAddress)).thenReturn(userDtos);

        ResponseEntity<ResponseWrapper> response = userController.getUserDetails(null, emailAddress);

        assertEquals(200, response.getStatusCodeValue());
        ResponseWrapper responseWrapper = response.getBody();
        assertNotNull(responseWrapper);
        List<UserDto> responseUserDtos = responseWrapper.getUserInfo();
        assertNotNull(responseUserDtos);
        assertEquals(1, responseUserDtos.size());
        assertEquals(emailAddress, responseUserDtos.get(0).getEmailAddress());
    }
}
