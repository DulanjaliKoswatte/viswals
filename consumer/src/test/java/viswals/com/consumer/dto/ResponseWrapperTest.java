package viswals.com.consumer.dto;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import viswals.com.consumer.entity.UserDto;

import java.util.ArrayList;
import java.util.List;

public class ResponseWrapperTest {

    private ResponseWrapper responseWrapper;

    @BeforeEach
    public void setUp() {
        responseWrapper = new ResponseWrapper();
    }

    @Test
    public void testGetSetUserInfo() {
        List<UserDto> userInfo = new ArrayList<>();
        UserDto userDto1 = new UserDto();
        userDto1.setId(1L);
        userDto1.setFirstName("John");
        UserDto userDto2 = new UserDto();
        userDto2.setId(2L);
        userDto2.setFirstName("Jane");
        userInfo.add(userDto1);
        userInfo.add(userDto2);

        responseWrapper.setUserInfo(userInfo);
        assertEquals(userInfo, responseWrapper.getUserInfo());
    }
}
