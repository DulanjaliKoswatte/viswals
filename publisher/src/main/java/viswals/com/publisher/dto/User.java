package viswals.com.publisher.dto;

import java.sql.Timestamp;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



@Getter
@Setter
@NoArgsConstructor
public class User {
    private Long id;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private Timestamp createdAt;
    private Timestamp deletedAt;
    private Timestamp mergedAt;
    private Long parentUserId;
}
