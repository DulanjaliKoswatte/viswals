package viswals.com.publisher.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    // Define the exchange
    @Bean
    public DirectExchange exchange() {
        return new DirectExchange("viswals-exchange");
    }

    // Define the queue
    @Bean
    @Qualifier("viswals_que_postgre")
    public Queue queuePostgre() {
        return new Queue("viswals_que_postgre");
    }

    @Bean
    @Qualifier("viswals_que_redis")
    public Queue queueRedis() {
        return new Queue("viswals_que_redis");
    }



    @Bean
    public Binding bindingPostgres(@Qualifier("viswals_que_postgre") Queue queue, DirectExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("viswals-routing-key");
    }

    @Bean
    public Binding bindingRedis(@Qualifier("viswals_que_redis") Queue queue, DirectExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("viswals-routing-key");
    }


}
