//package viswals.com.publisher.listners;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.ApplicationArguments;
//import org.springframework.boot.ApplicationRunner;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
//import org.springframework.stereotype.Component;
//import viswals.com.publisher.services.CsvToRabbitMQService;
//
//import java.io.IOException;
//
//@Component
//@ConditionalOnProperty(name = "listener.enabled", havingValue = "true")
//public class ApplicationStartupListener implements ApplicationRunner {
//
//    private final CsvToRabbitMQService csvService;
//
//    @Autowired
//    public ApplicationStartupListener(CsvToRabbitMQService csvService) {
//        this.csvService = csvService;
//    }
//
//    @Override
//    public void run(ApplicationArguments args) {
//        if (!args.containsOption("isUnitTest")) {
//            try {
//                Thread.sleep(10000);
//                csvService.readCSVandPublishToRabbitMQ();
//            } catch (IOException | InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//        }
//    }
//}
