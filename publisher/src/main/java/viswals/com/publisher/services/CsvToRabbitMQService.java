package viswals.com.publisher.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import viswals.com.publisher.dto.User;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
public class CsvToRabbitMQService {
    private final AmqpTemplate amqpTemplate;
    private final ObjectMapper objectMapper;

    @Value("${csv.filePathwithName}")
    String filePathWithName;

    @Value("${rabbitmq.exchangeName}")
    private String exchangeName;

    @Value("${rabbitmq.routingKey}")
    private String routingKey;

    private static final Logger LOGGER = LogManager.getLogger(CsvToRabbitMQService.class);
    @Autowired
    public CsvToRabbitMQService(AmqpTemplate amqpTemplate, ObjectMapper objectMapper) {
        this.amqpTemplate = amqpTemplate;
        this.objectMapper = objectMapper;
    }
    @GetMapping("/processCsv")
    public void readCSVandPublishToRabbitMQ() throws IOException {
        try(FileReader fileReader = new FileReader(ResourceUtils.getFile(filePathWithName));
            CSVParser csvParser = new CSVParser(fileReader, CSVFormat.DEFAULT)) {


            AtomicInteger ai = new AtomicInteger(0);
            List<Thread> threads = new ArrayList<>();
            for (CSVRecord csvRecord : csvParser) {
                int recordIndex = ai.getAndIncrement();
                Thread thread = new Thread(() -> {
                    try {
                        sentToRabbitMQ(csvRecord, recordIndex);
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                });
                threads.add(thread);
                thread.start();

            }
            for (Thread thread : threads) {
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    LOGGER.error("Thread interrupted: %s", e.getMessage());
                    Thread.currentThread().interrupt();
                }
            }
        } catch (IOException e) {
            LOGGER.error("Error reading CSV file: {}" , e.getMessage());
            throw  e;
        }
    }

    private void sentToRabbitMQ(CSVRecord csvRecord, int i) throws JsonProcessingException {
        try {
            if (i != 0) {
                User user = new User();
                user.setId(Long.parseLong(csvRecord.get(0)));
                user.setFirstName(csvRecord.get(1));
                user.setLastName(csvRecord.get(2));
                user.setEmailAddress(csvRecord.get(3));
                user.setCreatedAt(getDate(csvRecord.get(4)));
                user.setDeletedAt(getDate(csvRecord.get(5)));
                user.setMergedAt(getDate(csvRecord.get(6)));
                user.setParentUserId("-1".equals(csvRecord.get(7)) ? null : Long.parseLong(csvRecord.get(7)));
                String json = objectMapper.writeValueAsString(user);
                LOGGER.info("csvData : {}" , json);
                amqpTemplate.convertAndSend(exchangeName, routingKey, json);
            }

        } catch (IOException e) {
            LOGGER.error("Error processing CSV record: {}" , e.getMessage());
            throw e;
        }
    }

    private Timestamp getDate(String date){
        if(date.equalsIgnoreCase("-1")){
            return null;
        }else{
            return new Timestamp(Long.parseLong(date));
        }
    }

}
