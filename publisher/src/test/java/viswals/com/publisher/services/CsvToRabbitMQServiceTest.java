package viswals.com.publisher.services;

import org.apache.commons.csv.CSVFormat;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
@RunWith(SpringRunner.class)
@SpringBootTest
class CsvToRabbitMQServiceTest {


    @Test
    void testReadCSVandPublishToRabbitMQ() throws IOException {

        String csvString = "1,John,Doe,johndoe@example.com,1633000000000,1634000000000,1635000000000,2";
        CSVParser csvParser = CSVParser.parse(csvString, CSVFormat.DEFAULT);

        AmqpTemplate amqpTemplate = mock(AmqpTemplate.class);
        for (CSVRecord csvRecord : csvParser) {
            String id = csvRecord.get(0);
            String firstName = csvRecord.get(1);
            String lastName = csvRecord.get(2);
            String emailAddress = csvRecord.get(3);
            String createdAt = csvRecord.get(4);
            String deletedAt = csvRecord.get(5);
            String mergedAt = csvRecord.get(6);
            String parentUserId = csvRecord.get(7);

            assertNotNull(id);
            assertNotNull(firstName);
            assertNotNull(lastName);
            assertNotNull(emailAddress);
            assertNotNull(createdAt);
            assertNotNull(deletedAt);
            assertNotNull(mergedAt);
            assertNotNull(parentUserId);

            String json = buildJsonFromCsvData(id, firstName, lastName, emailAddress, createdAt, deletedAt, mergedAt, parentUserId);
            amqpTemplate.convertAndSend(anyString(), anyString(), eq(json));
        }
    }

    private String buildJsonFromCsvData(String id, String firstName, String lastName, String emailAddress,
                                        String createdAt, String deletedAt, String mergedAt, String parentUserId) {
        return "{"
                + "\"id\":\"" + id + "\","
                + "\"firstName\":\"" + firstName + "\","
                + "\"lastName\":\"" + lastName + "\","
                + "\"emailAddress\":\"" + emailAddress + "\","
                + "\"createdAt\":\"" + createdAt + "\","
                + "\"deletedAt\":\"" + deletedAt + "\","
                + "\"mergedAt\":\"" + mergedAt + "\","
                + "\"parentUserId\":\"" + parentUserId + "\""
                + "}";
    }
}
