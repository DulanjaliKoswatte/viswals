package viswals.com.publisher.dto;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = "listener.enabled=false")
public class UserTest {

    private User user;

    @BeforeEach
    public void setUp() {
        user = new User();
    }

    @Test
    public void testGetSetId() {
        Long id = 1L;
        user.setId(id);
        assertEquals(id, user.getId());
    }

    @Test
    public void testGetSetFirstName() {
        String firstName = "John";
        user.setFirstName(firstName);
        assertEquals(firstName, user.getFirstName());
    }

    @Test
    public void testGetSetLastName() {
        String lastName = "Doe";
        user.setLastName(lastName);
        assertEquals(lastName, user.getLastName());
    }

    @Test
    public void testGetSetEmailAddress() {
        String emailAddress = "john.doe@example.com";
        user.setEmailAddress(emailAddress);
        assertEquals(emailAddress, user.getEmailAddress());
    }

    @Test
    public void testGetSetCreatedAt() {
        Timestamp createdAt = Timestamp.valueOf("2023-01-01 00:00:00");
        user.setCreatedAt(createdAt);
        assertEquals(createdAt, user.getCreatedAt());
    }

    @Test
    public void testGetSetDeletedAt() {
        Timestamp deletedAt = Timestamp.valueOf("2023-02-01 00:00:00");
        user.setDeletedAt(deletedAt);
        assertEquals(deletedAt, user.getDeletedAt());
    }

    @Test
    public void testGetSetMergedAt() {
        Timestamp mergedAt = Timestamp.valueOf("2023-03-01 00:00:00");
        user.setMergedAt(mergedAt);
        assertEquals(mergedAt, user.getMergedAt());
    }

    @Test
    public void testGetSetParentUserId() {
        Long parentUserId = 2L;
        user.setParentUserId(parentUserId);
        assertEquals(parentUserId, user.getParentUserId());
    }
}
